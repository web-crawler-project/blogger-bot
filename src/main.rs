extern crate fetch_article;

use std::error::Error;
use ini::Ini;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // ini parse start
    let conf: Ini = Ini::load_from_file("config/main.ini").unwrap();

    let section = conf.section(Some("settings")).unwrap();
    let url = section.get("ARTICLE_URL").unwrap();
    // ini parse end

    fetch_article::fetch_articles(url).await?;
    Ok(())
}
