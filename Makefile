# Default target
prod: copy_config_prod build_release copy_to_root
test: copy_config_test build_release copy_to_root

# Copy the config file
copy_config_test:
	@cp -u config/test.ini config/main.ini
copy_config_prod:
	@cp -u config/prod.ini config/main.ini

# Build the Rust project in release mode
build_release:
	@cargo build --release

# Copy the release binary to the root directory and copy the config directory
copy_to_root:
	@if [ ! -d build ]; then mkdir build; fi
	@cp -u target/release/blogger-bot ./build
	@cp -ru config ./build/config

.PHONY: prod copy_config_prod build_release copy_to_root
.PHONY: test copy_config_test build_release copy_to_root
