use scraper::{Html, Selector};
use reqwest::Error;
use html2text::from_read;

// remove result html syntax
fn remove_html_tags(html_content: &str) -> String {
    let mut text_content = from_read(html_content.as_bytes(), 80); // 80 karakter sınırı ile metni alalım

    text_content = text_content.trim().to_string();

    text_content
}

// fetch article content
pub async fn fetch_articles(url: &str) -> Result<(), Error> {

    let body = reqwest::get(url).await?.text().await?; 

    let document = Html::parse_document(&body);

    let article_selector = Selector::parse("h3.post-title").unwrap();
    let category_selector = Selector::parse("div.post-body").unwrap();

    let mut article_titles = vec![];
    let mut article_categories = vec![];

    for title in document.select(&article_selector) {
        article_titles.push(title.inner_html());
    }

    for category in document.select(&category_selector) {
        article_categories.push(category.inner_html());
    }

    println!("empty:");
    for title in article_titles {
        println!(" - {}", title);
    }

    println!("\nContents:");
    for category in &article_categories {
        println!(" - {}", remove_html_tags(category));
    }

    Ok(())
}

